FROM maven:3.6.3-jdk-11-slim as MAVEN_BUILD

MAINTAINER Jonney

COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/
RUN mvn clean install && mvn package -B

FROM openjdk:11-slim
COPY --from=MAVEN_BUILD /build/target/data-collector-*.jar /app/data-collector.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/app/data-collector.jar"]
