package com.example.datacollector.configuration

import com.example.datacollector.model.DataCrawling
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import javax.annotation.PostConstruct

@Configuration
@EnableScheduling
class AppConfig(
    @Autowired var dataCrawling: DataCrawling
) {
    private val logger = LoggerFactory.getLogger(javaClass)

    @PostConstruct // run task after starting up
    @Scheduled(cron = "0 0 10 * * *") // trigger at 10AM everyday
    fun crawlData() {
        logger.info("Calling the scheduled task")
        dataCrawling.crawlData()
    }
}