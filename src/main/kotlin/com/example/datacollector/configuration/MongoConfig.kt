package com.example.datacollector.configuration

import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import java.util.*

@Configuration
@EnableMongoRepositories(basePackages = ["com.example.datacollector.repository"])
class MongoConfig(@Autowired var env: Environment) : AbstractMongoClientConfiguration() {
    override fun getDatabaseName(): String {
        return "covid_info"
    }

    override fun mongoClient(): MongoClient {
        val uri = env.getProperty("spring.data.mongodb.uri", "mongodb://localhost:27117/covid_info")
        val connectionString = ConnectionString(uri)
        val mongoClientSettings = MongoClientSettings.builder().applyConnectionString(connectionString).build()

        return MongoClients.create(mongoClientSettings)
    }

    override fun getMappingBasePackages(): MutableCollection<String> {
        return Collections.singleton(("com.example.datacollector.repository"))
    }
}