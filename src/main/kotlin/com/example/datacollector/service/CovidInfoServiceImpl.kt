package com.example.datacollector.service

import com.example.datacollector.model.CovidInfo
import com.example.datacollector.repository.CovidInfoRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class CovidInfoServiceImpl(
    @Autowired
    var covidInfoRepository: CovidInfoRepository
) : CovidInfoService {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getAllCovidInfo(): ArrayList<CovidInfo> {
        return covidInfoRepository.findAll() as ArrayList<CovidInfo>
    }

    override fun getCovidInfoByCountry(country: String): ArrayList<CovidInfo> {
        return covidInfoRepository.findByCountry(country)
    }

    override fun getCovidInfoByDate(date: String): ArrayList<CovidInfo> {
        return covidInfoRepository.findByDate(date)
    }

    override fun getCovidInfoByCountryAndDate(country: String, date: String): CovidInfo? {
        return covidInfoRepository.findByCountryAndDate(country, date)
    }

    override fun save(covidInfo: CovidInfo): Boolean {
        val item = covidInfoRepository.findByCountryAndDate(covidInfo.country, covidInfo.date)

        if (item != null) {
            logger.debug("Already exist item {}", item)
            return false
        }

        covidInfoRepository.save(covidInfo)
        logger.info("saved item to the database")
        logger.debug("item: {}", covidInfo)

        return true
    }
}