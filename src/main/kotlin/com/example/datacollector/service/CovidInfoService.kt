package com.example.datacollector.service

import com.example.datacollector.model.CovidInfo

interface CovidInfoService {
    fun getAllCovidInfo(): ArrayList<CovidInfo>
    fun getCovidInfoByCountry(country: String): ArrayList<CovidInfo>
    fun getCovidInfoByDate(date: String): ArrayList<CovidInfo>
    fun getCovidInfoByCountryAndDate(country: String, date: String): CovidInfo?
    fun save(covidInfo: CovidInfo): Boolean
}