package com.example.datacollector.model

import com.example.datacollector.service.CovidInfoServiceImpl
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component
import java.text.NumberFormat
import java.text.ParseException
import java.util.*

@Component
class DataCrawling(
    @Autowired var covidInfoServiceImpl: CovidInfoServiceImpl,
    @Autowired var kafkaTemplate: KafkaTemplate<String, String>
) {
    private val logger = LoggerFactory.getLogger(javaClass)

    fun crawlData() {
        val dataUrl = "https://www.worldometers.info/coronavirus/#countries"
        var covidInfoList: ArrayList<CovidInfo> = ArrayList<CovidInfo>()

        try {
            logger.info("Crawling data...")
            val doc = Jsoup.connect(dataUrl).timeout(60 * 1000).get()
            val table = doc.getElementById("main_table_countries_today")
            if (table != null) {
                covidInfoList = extractData(table)
                logger.info("covidInfoList: {}", covidInfoList)
            }
        } catch (e: Exception) {
            logger.error("Got error while crawling data...")
            logger.error(e.message)
            crawlData()
        }

        handleCovidInfoData(covidInfoList)
    }

    private fun handleCovidInfoData(covidInfoList: ArrayList<CovidInfo>) {
        covidInfoList.parallelStream().forEach lit@{ item ->
            if (!covidInfoServiceImpl.save(item)) {
                logger.info("Information for country: {}, date: {} is already existed!", item.country, item.date)
                return@lit
            }
            logger.info("Publish data to the kafka broker")
            kafkaTemplate.send("daily-covid-info", item.toString())
        }
    }

    private fun extractData(table: Element): java.util.ArrayList<CovidInfo> {
        var covidInfoList: java.util.ArrayList<CovidInfo> = java.util.ArrayList()
        val rows = table.select("tr").parallelStream().forEach { row ->
            row.getElementsByClass("mt_a").map { item ->
                val country = item.text()
                val cols = row.select("td")
                val cases = getDoubleFromString(cols[2].text())
                val deaths = getDoubleFromString(cols[4].text())
                val recovered = getDoubleFromString(cols[6].text())
                val covidInfo = CovidInfo(
                    country = country,
                    cases = cases,
                    deaths = deaths,
                    recovered = recovered
                )
                covidInfoList.add(covidInfo)
            }
        }

        return covidInfoList
    }

    private fun getDoubleFromString(stringNumber: String): Double {
        val format = NumberFormat.getInstance(Locale.US)
        var number: Number = 0
        try {
            number = format.parse(stringNumber)
        } catch (e: ParseException) {
            // logger.error("Got error while parsing {}", stringNumber)
            number = 0
        }
        return number.toDouble()
    }
}