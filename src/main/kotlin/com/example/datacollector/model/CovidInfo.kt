package com.example.datacollector.model

import com.google.gson.Gson
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

@Document("covid_info")
data class CovidInfo(
    @Id
    val id: ObjectId = ObjectId.get(),
    val country: String,
    val cases: Double,
    val deaths: Double,
    val recovered: Double,
    val date: String = LocalDate.now().toString()
) {
    override fun toString(): String {
        return Gson().toJson(this)
    }
}
