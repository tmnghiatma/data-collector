package com.example.datacollector.repository

import com.example.datacollector.model.CovidInfo
import org.springframework.data.mongodb.repository.MongoRepository

interface CovidInfoRepository : MongoRepository<CovidInfo, String> {
    fun findByCountry(country: String): ArrayList<CovidInfo>
    fun findByDate(date: String): ArrayList<CovidInfo>
    fun findByCountryAndDate(country: String, date: String): CovidInfo?
}